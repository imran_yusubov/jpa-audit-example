package az.ingress.audit.jpaaudit.repository;

import az.ingress.audit.jpaaudit.entity.Car;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarRepository extends JpaRepository<Car, Long> {
}
