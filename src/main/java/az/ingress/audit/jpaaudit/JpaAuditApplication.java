package az.ingress.audit.jpaaudit;

import az.ingress.audit.jpaaudit.service.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class JpaAuditApplication implements CommandLineRunner {

    private final CarService carService;

    public static void main(String[] args) {
        SpringApplication.run(JpaAuditApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
		carService.createCar();
    }
}
