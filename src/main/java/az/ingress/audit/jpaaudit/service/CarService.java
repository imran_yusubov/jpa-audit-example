package az.ingress.audit.jpaaudit.service;

import az.ingress.audit.jpaaudit.entity.Car;
import az.ingress.audit.jpaaudit.repository.CarRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CarService {

    private final CarRepository carRepository;

    public void createCar() {
        Car car = new Car();
        car.setName("BMW");

        carRepository.save(car);
    }
}
