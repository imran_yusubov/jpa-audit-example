package az.ingress.audit.jpaaudit.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class CarAuditService {

    public void auditing(Object o) {
        log.info("Write any logic here {}", o);
    }
}
