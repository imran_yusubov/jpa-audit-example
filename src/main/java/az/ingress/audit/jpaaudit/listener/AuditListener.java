package az.ingress.audit.jpaaudit.listener;

import az.ingress.audit.jpaaudit.service.CarAuditService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RequiredArgsConstructor
public class AuditListener {

    private final CarAuditService carAuditService;

    private static final Map<String, Object> mapReference = new HashMap<>() {
        private static final long serialVersionUID = -621065317395536495L;
    };

    @PostLoad
    public void setupPreviousFields(Object loggedModel) {

    }

    @PrePersist
    @PreUpdate
    @PreRemove
    private void beforeAnyOperation(Object object) {
        log.info("Auditing {}", object);
        carAuditService.auditing(object);
    }
}